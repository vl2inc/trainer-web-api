#!/bin/bash


general_path=/home/ec2-user/lundero/trainer-web-api/src
out_file=${general_path}/out.txt

SECONDS=0

echo "Inicio:" > ${out_file}
date >> ${out_file}
echo -e "\n\n" >> ${out_file}
#echo -e "GET:\n" >> ${out_file}
#curl -X GET http://localhost:8088/api/models/100-5/training >> ${out_file}
#echo -e "\n\nPOST:\n" >> ${out_file}
#curl -X POST http://localhost:8088/api/models/100-5/training >> ${out_file}
echo -e "\nTraining test script:\n" >> ${out_file}
source ~/.bashrc
source activate tensorflow
python /home/ec2-user/lundero/trainer-web-api/src/training_time.py >> ${out_file}
echo -e "\n\n" >> ${out_file}
echo "Fin:" >> ${out_file}
date >> ${out_file}

duration=$SECONDS
echo -e "\n$(($duration / 60)) minutos y $(($duration % 60)) segundos.\n" >>${out_file}
