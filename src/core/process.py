import os
import itertools
import http.client
import numpy as np
from keras import backend as K

from mongoengine import connect
from core.db import IntervalDocument, RunDocument, ModelDocument
from core.settings import DB_NAME
from core.neural_network_preprocessing import launch_preprocessing_data_nn
from core.settings import PERCENT_TEST, DB_NAME
from core.train import train


def _get_intervals():
    connect(DB_NAME)
    intervals = IntervalDocument.objects()
    return intervals


def _iterate_intervals(mgr_id,df_num,df_cat,df_lbls,df_num_t,df_cat_t,df_lbls_t,tipo_modelo,num_clases,intervals,mongo_model):
    run_id = 1
    runs = []

    learning_rate_exp = next(x for x in intervals if x.name == 'learning_rate_exp')
    num_steps = next(x for x in intervals if x.name == 'num_steps')
    dropout_rate = next(x for x in intervals if x.name == 'dropout_rate')
    training_epochs = next(x for x in intervals if x.name == 'training_epochs')

    lr_array = np.arange(learning_rate_exp.start, learning_rate_exp.end + learning_rate_exp.jump, learning_rate_exp.jump)
    epochs_array = np.arange(training_epochs.start, training_epochs.end + training_epochs.jump, training_epochs.jump)
    dropout_array = np.arange(dropout_rate.start, dropout_rate.end + dropout_rate.jump, dropout_rate.jump)
    steps_array = np.arange(num_steps.start, num_steps.end + num_steps.jump, num_steps.jump)

    for lr, te, dr, steps in itertools.product(lr_array, epochs_array, dropout_array, steps_array):
        _lr = pow(10, float(lr))
        _te = int(te)
        _dr = round(float(dr), 1)
        _steps = int(steps)

        print('STARTING NEW RUN FOR:', mgr_id)
        print(_lr, _te, _dr, _steps)
        print('tipo_modelo:', tipo_modelo)
        print('num_clases:', num_clases)
        print('lr:', _lr)
        print('training_epochs:', _te)
        print('num_steps:', _steps)
        print('dropout_rate:', _dr)
        print('mgr_id:', mgr_id)
        print('run_id:', run_id)

        K.clear_session()
        loss, accuracy = train(df_num,df_cat,df_lbls,df_num_t,df_cat_t,df_lbls_t,tipo_modelo,num_clases,_lr,_te,_steps,_dr,mgr_id,str(run_id))

        print("Saving run")
        run = RunDocument()
        run.run_id = str(run_id)
        run.learning_rate_exp = _lr
        run.dropout_rate = _dr
        run.num_steps = _steps
        run.training_epochs = _te
        run.loss = loss
        run.accuracy = accuracy

        mongo_model.runs.append(run)

        run = {
            'run_id': str(run_id),
            'learning_rate_exp': _lr,
            'training_epochs': _te,
            'dropout_rate': _dr,
            'num_steps': _steps,
            'loss': float(loss),
            'accuracy': float(accuracy)
        }
        runs.append(run)

        run_id = run_id + 1

    best_run = min(runs, key=lambda x: x['loss'])

    print("Best run:")
    print('run_id:', best_run['run_id'])
    print('accuracy:', best_run['accuracy'])
    print('loss:', best_run['loss'])
    print('FINISHED TRAINING FOR:', mgr_id)

    return best_run


def _delete_model(mgr_id):
    models = ModelDocument.objects(MGR_ID=mgr_id)

    if models.count() == 0:
        return

    models.delete()


def _message_sockets(mgr_id):
    print('Sending _message_sockets', mgr_id)
    conn =  http.client.HTTPConnection('localhost', 8088)
    conn.request('POST', '/api/models/ws/' + mgr_id)
    conn.getresponse()


def start_training(mgr_id):
    try:
        print('STARTING TRAINING FOR:', mgr_id)
        intervals = _get_intervals()

        _delete_model(mgr_id)
  
        metadata_file = 'metadata_' + mgr_id
        sys_flag = 2

        print('STARTING PREPROCESSING FOR:', mgr_id)
        print('mgr_id', mgr_id)
        print('metadata_file:', metadata_file)
        print('percent_test', PERCENT_TEST)
        print('sys_flag:', sys_flag)

        df_cat,df_num,df_lbls,df_cat_t,df_num_t,df_lbls_t,num_feat,num_clases,tipo_label = launch_preprocessing_data_nn(mgr_id, PERCENT_TEST, sys_flag)

        print('FINISHED PREPROCESSING FOR:', mgr_id)
        print('tipo_label:', tipo_label)

        tipo_modelo = 'clasificator' if (tipo_label == 'cualitativo' or tipo_label == 'binario') else 'regressor'

        print('tipo_modelo:', tipo_modelo)

        temp_cat = df_cat#.astype('float32')
        temp_cat_t = df_cat_t#.astype('float32')

        connect(DB_NAME)
        models = ModelDocument.objects(MGR_ID=mgr_id)
        model = models[0]

        best_run = _iterate_intervals(mgr_id,df_num,temp_cat,df_lbls,df_num_t,temp_cat_t,df_lbls_t,tipo_modelo,num_clases,intervals,model)
        print('Saving results')
        model.tipo_modelo = tipo_modelo
        best_run = RunDocument(**best_run)
        model.best_run = best_run
        model.save()

    except Exception as e:
        print('Unexpected error:')
        print(e)
