
# coding: utf-8

# In[13]:


#df_categoric, df_numeric, df_labels, df_categoric_test, df_numeric_test, df_labels_test, num_features, num_clases, tipo_label= launch_preprocessing_data_nn("100-5", 0.9, 2)


# In[18]:


import pandas as pd
import numpy as np
from pymongo import MongoClient
import os
from decimal import Decimal
import io
from sklearn.model_selection import train_test_split
import boto3
import configparser
import urllib.request

config = configparser.ConfigParser()
path_directory = os.getcwd()
config.read(path_directory + "/config.ini")

def ConfigSectionMap(section):
    dict1 = {}
    options = config.options(section)
    for option in options:
        try:
            dict1[option] = config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

def launch_preprocessing_data_nn (MGR_ID, percent_test, flag_file_system):
    
    Id_MGR=MGR_ID
    #metadata_path = pathmetadata ##"D:/Usuarios/cpua/Documents/Migrar a nueva PC/Personal Things/el2/Proyecto Lundero-Redes Neuronales/metadata_cuantitativo.csv"
    #data_path = pathdata ##"D:/Usuarios/cpua/Documents/Migrar a nueva PC/Personal Things/el2/Proyecto Lundero-Redes Neuronales/DataRedNeuronalCuantitativo.csv"
    
    local_path = ConfigSectionMap("SectionOne")['local_path']
    bucket_name = ConfigSectionMap("SectionOne")['bucket_name']
    prefix_name = ConfigSectionMap("SectionOne")['prefix_name']
    mongo_uri = ConfigSectionMap("SectionOne")['mongo_uri']
    data_separator = ConfigSectionMap("SectionOne")['data_separator']
    metadata_separator = ConfigSectionMap("SectionOne")['metadata_separator']    
    
    
    if flag_file_system == 1:
        ##reading csv files

        metadata_path = local_path + "metadata_" + Id_MGR  + ".csv"
        data_path = local_path + Id_MGR + ".csv"

        df_meta = pd.read_csv(metadata_path,sep=metadata_separator,skiprows=0)
        df_data = pd.read_csv(data_path, sep=data_separator, dtype=str, skiprows=0, encoding='latin-1')

    else:
        
        METADATA = "metadata_" + Id_MGR

        #BUCKET = bucket_name

        KEY= Id_MGR + ".csv"
        KEY_METADATA =METADATA + ".csv"

        #s3 = boto3.client('s3')
        #obj = s3.get_object(Bucket=BUCKET, Key=KEY)
        #obj2 = s3.get_object(Bucket=BUCKET, Key=KEY_METADATA)

        #df_meta = pd.read_csv(io.BytesIO(obj2['Body'].read()),sep=metadata_separator,skiprows=0)

        #df_data = pd.read_csv(io.BytesIO(obj['Body'].read()), sep=data_separator, dtype=str, skiprows=0, encoding='latin-1')     
        obj = urllib.request.urlopen('https://lundero-data-test-train.s3.amazonaws.com/files/' + KEY)
        obj2 = urllib.request.urlopen('https://lundero-data-test-train.s3.amazonaws.com/files/' + KEY_METADATA)
        df_data = pd.read_csv(io.BytesIO(obj.read()), sep=data_separator, dtype=str, skiprows=0, encoding='latin-1')     
        df_meta = pd.read_csv(io.BytesIO(obj2.read()),sep=metadata_separator,skiprows=0)
        

    tipo_modelo=df_meta.tipo.iloc[-1] #binario, cualitativo, cuantitativo
    
    df_data.fillna("-1", inplace=True)
    last_column=df_data.iloc[:,-1]


    ############split data into train and test ###################

    long = len(df_data)

    seed=[1,long]
    train_percent=percent_test
    np.random.seed(seed)
    perm = np.random.permutation(df_data.index)
    m = len(df_data.index)
    train_end = int(train_percent * m)

    train = df_data.iloc[perm[:train_end]]
    validate = df_data.iloc[perm[train_end:]]


    df_data=train.copy()
    df_validate = validate.copy()
    ##############################################################



    rows_ini, columns_ini = df_data.shape
    last_column_name = df_data.columns[columns_ini-1]


    ####### change dataframe into category type ############
    cols_to_transform = [] 
    cols_numeric = []
    cols_label = []
    for index, row_meta in df_meta.iterrows():
            nombre=row_meta[0]
            #df_data[str(nombre)] = df_data[str(nombre)].str.replace('[^\w\s]','')
            if row_meta[1] == "categorico":            
                df_data[str(nombre)] = df_data[str(nombre)].astype('category')
                cols_to_transform.append(nombre)
            elif row_meta[1] == "numerico":
                cols_numeric.append(nombre)
            else:
                cols_label.append(nombre)
    ####### end change dataframe into category type ############


    ####### label encoding for categoric columns ##########
    df_data_mongo = df_data.copy()           

    df_data[cols_to_transform] = df_data[cols_to_transform].apply(lambda x: x.cat.codes)

    #dfret = df_data.copy()

    ####### end label encoding for categoric columns ##########

    ###### creating ranges by quantiles for cuantitative data #########

    #if tipo_modelo == "cuantitativo":
    #    df_cat_num=[]
    #    df_cat_num_mongo = []
    #    for index, row_meta in df_meta.iterrows():
    #        nombre=row_meta[0]             
    #        if row_meta[1] == "numerico":                
    #            df_data[str(nombre)] = df_data[str(nombre)].apply(lambda x: float(x.split()[0].replace(',', '')))

    #            dfq = df_data.copy()

    #            df_data = df_data.drop(columns=str(nombre))
    #            quantile_list = [0, .25, .5, .75, 1.]
    #            quantiles = dfq[str(nombre)].quantile(quantile_list)

    #            quantile_labels = ['1', '2', '3', '4']
    #            dfq['Income_quantile_range'] = pd.qcut(
    #                                                dfq[str(nombre)], 
    #                                                q=quantile_list)
    #            dfq[str(nombre)] = pd.qcut(
    #                                                dfq[str(nombre)], 
    #                                                q=quantile_list,       
    #                                                labels=quantile_labels)
    #
    #            df_data = df_data.join(dfq[[str(nombre)]])


    #    df_data = df_data.drop(columns=last_column_name)
    #    df_data = df_data.join(dfq[last_column_name])
    ###### end creating ranges by quantiles for cuantitative data #########    

    ###### one hot encoding ##############
    columns_one_hot=0
    columns_final=0
    one_hot = []
    columns = 0
    df = pd.DataFrame(df_data)
    if tipo_modelo != "cuantitativo": #hot enconding for labels, only apply for binary and qualitative
        rows, columns = df.shape
        name = df.columns[columns-1]
        dfcatalogolabels = df[name]
        one_hot = pd.get_dummies(df[name])
        rows_one_hot, columns_one_hot = one_hot.shape
        df = df.drop(name, axis=1)
        df_datafinal = df.join(one_hot)

        dfcatlab = pd.DataFrame(dfcatalogolabels)
        dflabcat = dfcatlab.join(one_hot)
    else:    
        df_datafinal = df

    ###



    ###

    rows_final, columns_final = df_datafinal.shape

    num_features = columns_final-columns_one_hot
    num_clases = columns_one_hot
    ####### end one hot encoding ###################


    ###### creating label dictionary to sabe it in mongodb e.g. binnary 1: 10, 0: 01 ###########

    cate=[]
    if tipo_modelo != "cuantitativo":
        df_labels=pd.DataFrame(df_data_mongo.iloc[:,-1])
        df_one = pd.DataFrame(one_hot)
        df_label_header= df_one.columns.values

        df_concat=pd.DataFrame(df_one[df_label_header].astype(str).apply(''.join, axis=1))

        df_catalogo_labels=df_labels.join(df_concat)

        cate=df_catalogo_labels.values

    else:
        cate=[]  
    ###### end of creating label dictionary to sabe it in mongodb e.g. binnary 1: 10, 0: 01 ###########


    df_lab = pd.DataFrame(cate)

    df_categoric = df_datafinal[cols_to_transform].copy()
    df_numeric = df_datafinal[cols_numeric].copy()
    #if tipo_modelo != "cuantitativo":
    df_lab = df_datafinal.iloc[:, columns-1:columns_final].values  #df_catalogo_labels.iloc[:,-1].values
    #else:
    #    df_lab = ['0']
    df_labels = pd.DataFrame(df_lab)

    df_categoric=df_categoric.astype('object')
    df_numeric=df_numeric.astype('object')
    df_labels=df_labels.astype('object')


    #split data into train and test
    ###### categoric data ########
    #X = df_categoric.iloc[:, :columns-1].values  
    #y = df_categoric.iloc[:, columns-1:columns_final].values  

    #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=1, random_state=0)  

    #traindata = np.hstack((X_train, y_train))
    #testdata  = np.hstack((X_test, y_test))


    #cat_test_data = pd.DataFrame(testdata) 
    #cat_train_data = pd.DataFrame(traindata)   

    #cat_test_data = cat_test_data.astype('object')
    #cat_train_data = cat_train_data.astype('object')


    ###### numeric data #######
    #X = df_numeric.iloc[:, :columns-1].values  
    #y = df_numeric.iloc[:, columns-1:columns_final].values  

    #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=1, random_state=0)  

    #traindata = np.hstack((X_train, y_train))
    #testdata  = np.hstack((X_test, y_test))

    #num_test_data = pd.DataFrame(testdata) 
    #num_train_data = pd.DataFrame(traindata) 

    #num_test_data = num_test_data.astype('object')
    #num_train_data = num_train_data.astype('object')


    ###### label data ######
    #X = df_labels.iloc[:, :columns-1].values  
    #y = df_labels.iloc[:, columns-1:columns_final].values  

    #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=1, random_state=0)  

    #traindata = np.hstack((X_train, y_train))
    #testdata  = np.hstack((X_test, y_test))

    #lab_test_data = pd.DataFrame(testdata) 
    #lab_train_data = pd.DataFrame(traindata) 

    #lab_test_data = lab_test_data.astype('object')
    #lab_train_data = lab_train_data.astype('object')


    tipo_label = tipo_modelo


    #print(validate)

    # ====== Connection ====== #
    # Connection to Mongo


    MONGO_URI = mongo_uri



    client = MongoClient(MONGO_URI)
    db = client.neural_network
    collection = db.dictionary

    collection.find ()

    coleccion = {}
    dictionary = []
    dictionary_2 = {'Columns':{}}
    numeric_dict = []
    coleccion['MGR_ID'] = Id_MGR 
    coleccion['Num_Features'] = num_features
    coleccion['Num_Labels'] = num_clases 
    coleccion['Catalogos'] = [] 
    coleccion['Labels'] = []
    coleccion['Labels'].append({"Diccionario" : dict(cate)})
    for index, row_meta in df_meta.iterrows():
        nombre=row_meta[0]
        if row_meta[1] == "categorico":        
            dictionary = dict((str(category), code) for code, category in enumerate(df_data_mongo[str(nombre)].cat.categories))        
            dictionary_2['Columns'][nombre]= dictionary

            coleccion['Catalogos'].append({"Atributo" : str(nombre), "Diccionario" : dictionary})       
        #elif row_meta[1] == "numerico":    
        #    df_data_mongo[str(nombre)] = df_data_mongo[str(nombre)].apply(lambda x: float(x.split()[0].replace(',', '')))

        #    df_data_mongo2 = df_data_mongo.copy()

        #    if tipo_modelo == "cuantitativo":


        #        quantile_list = [0, .25, .5, .75, 1.]
        #        quantiles = df_data_mongo2[str(nombre)].quantile(quantile_list)

        #       quantile_labels = ['1', '2', '3', '4']
        #        df_data_mongo2['range'] = pd.qcut(
        #                                            df_data_mongo2[str(nombre)], 
        #                                            q=quantile_list)
        #        df_data_mongo2['codigo'] = pd.qcut(
        #                                            df_data_mongo2[str(nombre)], 
        #                                            q=quantile_list,       
        #                                            labels=quantile_labels)

        #       numeric_dict = df_data_mongo2[['range', 'codigo']]

        #        df2 = pd.DataFrame(numeric_dict.drop_duplicates())

        #        diction = {}
        #        for index, row_meta in df2.iterrows():
        #            codigo=str(row_meta[1])
        #            rango =str(row_meta[0])
        #            diction[str(codigo)]=str(rango)


                #coleccion['Catalogo_num'].append({"Atributo" : str(nombre), "Diccionario" : diction})    

    collection.insert(coleccion, check_keys=False)
   
    labdic=dict(cate)
    #labcat=dict(dictionary_2['Columns'])
    #print(labdic)
    #return
    for index, row_meta in df_meta.iterrows():   
        nombre = str(row_meta[0])
        
        #df_validate[str(nombre)] = df_validate[str(nombre)].str.replace('[^\w\s]','')
        if row_meta[1] == "categorico":
            data = dictionary_2['Columns'][nombre]
            #print(nombre)
            for index_val, row_meta_val in df_validate.iterrows():
                dato= str(row_meta_val[nombre])                
                valor = data.get(dato, "")
                row_meta_val[nombre] = '-1' if valor == "" else valor
                #if nombre == "CODIGO_VENTAJA":                    
                #    print(row_meta_val[nombre])                
        elif row_meta[1]=="numerico":
            df_validate[str(nombre)] = df_validate[str(nombre)].str.replace('[^\w\s]','')
        else:
            for index_val, row_meta_val in df_validate.iterrows():
                dato= row_meta_val[nombre]
                             
                row_meta_val[nombre] = labdic.get(dato,"")
                #print(row_meta_val[nombre])
    
    #df_validate=df_validate.astype('float')
    #return
    labels = []
    new_columns = []
    
    #print(df_validate)
    

    #df_validate['list'] = df_validate.iloc[:,-1].str.split() 
    
    #df_validate.to_csv('/home/ec2-user/test.csv', mode='w', index=False)
    
    #print(df_validate)
    #return

    for index, row_meta in df_meta.iterrows():
        if not (row_meta[1] == "categorico" or row_meta[1]=="numerico"):
            for index_val, row_meta_val in df_validate.iterrows():
                #label = str(row_meta_val[index])
                labels.append({
                    'value': str(row_meta_val[index]),
                    'index': index_val
                })

    for i in range(num_clases):
        values = []
        indexes = []
        column_name = 'label_' + str(i)

        for label in labels:
            values.append(label['value'][i])
            indexes.append(label['index'])

        df = pd.DataFrame(values, columns=[column_name], index=indexes)
        df_validate = df_validate.join(df[column_name])
        
        

    df_validate = df_validate.drop(columns=str(last_column_name))

    df_categoric_test = df_validate[cols_to_transform].copy()
    df_numeric_test = df_validate[cols_numeric].copy()
    #if tipo_modelo != "cuantitativo":
    df_lab_test = df_validate.iloc[:, columns-1:columns_final].values  #df_catalogo_labels.iloc[:,-1].values
    #else:
    #    df_lab_test = ['0']
    df_labels_test = pd.DataFrame(df_lab_test)
    
    #df_categoric_test.to_csv('/home/ec2-user/test.csv', mode='w', index=False)
    #print(df_categoric_test)
    #return

    df_categoric_test =df_categoric_test.astype('float')
    df_numeric_test=df_numeric_test.astype('float')
    df_labels_test=df_labels_test.astype('float')
    df_categoric=df_categoric.astype('float')
    df_labels = df_labels.astype('float')
    df_numeric = df_numeric.astype('float')
    
    num_clases = 1 if tipo_modelo == "cuantitativo" else columns_one_hot
    
    return df_categoric, df_numeric, df_labels, df_categoric_test, df_numeric_test, df_labels_test, num_features, num_clases, tipo_label
    


# In[ ]:


def export_train_test_data_to_csv(test_data, train_data, path):
        
    
    filename="test_data.csv"
    test_data.to_csv(path + filename, header=False, mode='w', index=False)
    
    filename="train_data.csv"
    train_data.to_csv(path + filename, header=False, mode='w', index=False)
    
    return print("Files were exported in path " + path)


