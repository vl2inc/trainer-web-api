import os
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import r2_score
from sklearn.preprocessing import StandardScaler
from keras.models import Model
from keras.models import Sequential
from keras.layers import Dense, Activation, Reshape, merge, Embedding, Input, Dropout, BatchNormalization
from keras.layers import concatenate
from keras import layers
from keras import optimizers
from keras import backend as K
import tensorflow as tf
import pickle


def coeff_determination(y_true, y_pred):
    SS_res =  K.sum(K.square( y_true-y_pred )) 
    SS_tot = K.sum(K.square( y_true - K.mean(y_true) ) ) 
    return ( 1 - SS_res/SS_tot)


def build_layers(x_cat,num_numeric_features,num_clases,model_type,DropoutRate,lr): 
    inputs=[]
    to_concat=[]
    #print(type(x_cat))
    num_features=len(x_cat.columns)+ num_numeric_features
    
    
    if num_numeric_features > 0:
        aux_input = Input(shape=(num_numeric_features,), name='aux_input')
        inputs.append(aux_input)
        to_concat.append(aux_input)
    
    #iterate over the columns and create an embedding for each one
    for i in x_cat:
        size=len(x_cat[i].unique())+1
        name= "input_" + str(i)
        
        embedding_size = (size+1) // 2
        if embedding_size > 50: 
            embedding_size = 50
        
        e_input = Input(shape=(1,), dtype='float32', name=name)
        emb= Embedding(size, embedding_size, input_length=1)(e_input)
        e_out=Reshape(target_shape=(embedding_size,))(emb)
        
        inputs.append(e_input)
        to_concat.append(e_out)

    if model_type=="regressor":
        x = layers.concatenate(to_concat)

        x = Dense(int(num_features*1.8), kernel_initializer='uniform')(x)
        x = BatchNormalization()(x)
        x = Activation('relu')(x)
        x = Dropout(DropoutRate)(x)
        x = Dense(int(num_features*4), kernel_initializer='uniform')(x)
        x = BatchNormalization()(x)
        x = Activation('relu')(x)
        x = Dropout(DropoutRate)(x)
    else:
        x = layers.concatenate(to_concat)

        x = Dense(600, kernel_initializer='uniform')(x)
        #x = Dense(1000, kernel_initializer='normal')(x)


        x = BatchNormalization()(x)
        x = Activation('relu')(x)
        x= Dropout(DropoutRate)(x)

        #x = Dense(50)(x)
        #x= Activation('relu')(x)
        #x= Dropout(.15)(x)
        #x = Dense(800)(x)
        #x= Activation('relu')(x)

        #x = Dense(600)(x)
        #x= Activation('relu')(x)

        #x = Dense(400)(x)
        #x= Activation('relu')(x)

        #x= Dropout(DropoutRate)(x)
        x = Dense(500, kernel_initializer='uniform')(x)
        #x = Dense(500, kernel_initializer='normal')(x)

        x = BatchNormalization()(x)
        #x = Dense(10)(x)
        x= Activation('relu')(x)
        #x= Dropout(.5)(x)
        x= Dropout(DropoutRate)(x)
        
    
    if model_type=="regressor":
        print("regressor")
        output = Dense(num_clases, name="output")(x)
        model = Model(inputs=inputs, outputs=[output])
        model.compile(loss='mean_absolute_error',optimizer=optimizers.Adamax(lr=lr),metrics=[coeff_determination])
        #model.compile(loss='mean_squared_error',optimizer=optimizers.(lr=lr))
    #output = Dense(1, activation='sigmoid')(x)
    else:
        x = Dense(num_clases)(x)
        output= Activation('softmax' , name="output")(x)
        model = Model(inputs=inputs, outputs=[output])
        model.compile(loss='categorical_crossentropy',optimizer=optimizers.Adam(lr=lr),metrics=['acc'])
    return model


def train(train_num_df,train_cat_df,y_train,validation_num_df,validation_cat_df,y_validation,model_type
          ,num_clases,lr,training_epochs,num_steps,DropoutRate,model_id,run):   
    train_cat_df=train_cat_df +1
    validation_cat_df=validation_cat_df +1
    
    i=1
    directory = "models/"+ model_id
    
    if not os.path.exists(directory):
        os.makedirs(directory)
        
    subdirectory= directory + "/" + run +"/"
    if not os.path.exists(subdirectory):
        os.makedirs(subdirectory)    
      
    file_name= subdirectory +  run +  ".ckpt"
    
  
   
    num_numeric_features=train_num_df.shape[1]
    
    model=build_layers(train_cat_df,num_numeric_features,num_clases,model_type,DropoutRate,lr)
    
    #x_train=pd.concat([train_num_df,train_cat_df],axis=1).as_matrix()
    x_train=[]
    
    x_validation=[]
    ###### Standarization ###############################
    print("Start StandardScaler")
    
    e=StandardScaler()
    
    if not train_num_df.empty:
        train_num_df= e.fit_transform(train_num_df)
        x_train.append(np.array(train_num_df))
    if not validation_num_df.empty:
        validation_num_df= e.transform(validation_num_df)
        x_validation.append(np.array(validation_num_df))
    
        filehandler = open(subdirectory+"/stdscaler.obj","wb")
        pickle.dump(e,filehandler)
        filehandler.close()
    
    print("End StandardScaler")
    #####################################################################
        
    
    #x_train.append(np.array(train_num_df.as_matrix()))
    #x_validation.append(np.array(validation_num_df.as_matrix()))
    
  
    col_vals_dict = {c: list(train_cat_df[c].unique()) for c in train_cat_df.columns}

    embed_cols = []
    for c in col_vals_dict:
        embed_cols.append(c)
        #print(c , ': %d values' % len(col_vals_dict[c])) #look at value counts to know the embedding dimensions

    #the cols to be embedded: rescaling to range [0, # values)
    for c in embed_cols:
        raw_vals = np.unique(train_cat_df[c])
        val_map = {}
        
        #for i in range(len(raw_vals)):
        #    val_map[raw_vals[i]] = i
       
        
        #x_train.append(train_cat_df[c].map(val_map).values)
        #x_validation.append(validation_cat_df[c].map(val_map).fillna(0).values)
        x_train.append(np.asarray(train_cat_df[c]))
        x_validation.append(np.asarray(validation_cat_df[c]))

    
    #########################################################h.history['val_loss']
    
    y_train=y_train.as_matrix()
    y_validation=y_validation.as_matrix()
    
    # calculate the batch size
    batch_size= train_num_df.shape[0]// num_steps
    print("Batch size: ", batch_size)
    
    h=model.fit(x_train, y_train, validation_data=(x_validation,y_validation), epochs=training_epochs, batch_size=batch_size)
      
    model.save(subdirectory + '/model.h5')
    
    val_loss=h.history['val_loss'][-1]    
    loss=h.history['loss'][-1]
    
    if loss>val_loss:
        loss_=val_loss
    else:
        loss_=val_loss + ((val_loss-loss))

 
    if model_type=='regressor':
        acc=h.history['coeff_determination'][-1]
    else:
        acc=h.history['val_acc'][-1]
    
    print("loss:",loss_)
    print("Accuracy:", acc)
    
    del train_num_df,train_cat_df,y_train,validation_num_df,validation_cat_df,y_validation,model
        
    return loss_, acc
