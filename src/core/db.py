from mongoengine import Document, EmbeddedDocument, EmbeddedDocumentField, StringField, FloatField, IntField, ListField

class IntervalDocument(Document):
    name = StringField(required=True, unique=True)
    start = FloatField(required=True)
    end = FloatField(required=True)
    jump = FloatField(required=True)
    meta = {
        'collection': 'intervals'
    }

class RunDocument(EmbeddedDocument):
    run_id = StringField()
    learning_rate_exp = FloatField()
    dropout_rate = FloatField()
    num_steps = IntField()
    training_epochs = IntField()
    loss = FloatField()
    accuracy = FloatField()


class ModelDocument(Document):
    MGR_ID = StringField(required=True, unique=True)
    Num_Features = IntField()
    Num_Labels = IntField()
    best_run = EmbeddedDocumentField(required=False, document_type=RunDocument)
    runs = ListField(EmbeddedDocumentField(required=False, document_type=RunDocument))
    tipo_modelo = StringField()
    meta = {
        'collection': 'dictionary',
        'strict': False
    }
