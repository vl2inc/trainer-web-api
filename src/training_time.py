from core.process import start_training
import time

if __name__ == "__main__":
    results_file = open('results.txt', 'w') 
    print("Starting training time test") 
    #print("[CALL] Training for model: 100-5") 
    #start = time.time()
    #start_training('100-5')
    #end = time.time()
    #total = end - start
    #print("[FINISHED] Training for model: 100-5")
    #results_file.write(f"100-5: {total} seconds")

    print("[CALL] Training for model: 200-5")
    start = time.time()
    start_training('200-5')
    end = time.time()
    total = end - start
    print("[FINISHED] Training for model: 200-5")
    results_file.write(f"200-5: {total} seconds")

    #binario
    print("[CALL] Training for model: 300-5")
    start = time.time()
    start_training('300-5')
    end = time.time()
    total = end - start
    print("[FINISHED] Training for model: 100-5")
    results_file.write(f"300-5: {total} seconds")
