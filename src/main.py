from tornado.ioloop import IOLoop
from tornado.web import Application
from urls import URLS

if __name__ == "__main__":
    app = Application(URLS)
    app.listen(8088)
    IOLoop.current().start()
