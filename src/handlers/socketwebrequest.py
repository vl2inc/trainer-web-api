from tornado.web import RequestHandler
from handlers.trainingwebsocket import TrainingWebSocket
from tornado.web import RequestHandler
from core.process import start_training
from core.db import IntervalDocument, RunDocument, ModelDocument
from mongoengine import connect


class SocketWebRequest(RequestHandler):
    def post(self, mgr_id):
        print('Starting WebSocket message for finished')
        connect('neural_network')
        models = ModelDocument.objects(MGR_ID=mgr_id)

        if models.count() == 0:
            return

        model = models[0]

        model.status = 'finished' if model.run else 'running'

        #json =  json_encode(model)

        res = {
            'MGR_ID': model.MGR_ID,
            'status': model.status
        }

        if model.status == 'finished':
            res['loss'] = model.run.loss
            res['accuracy'] = model.run.accuracy
            res['metadata'] = {
                'learningRate': float(model.run.learning_rate_exp),
                'dropoutRate': model.run.dropout_rate,
                'numSteps': model.run.num_steps,
                'trainingEpochs': model.run.training_epochs
            }

        message = self.request.body
        TrainingWebSocket.send_model(mgr_id=mgr_id, message=res)
        self.finish()
