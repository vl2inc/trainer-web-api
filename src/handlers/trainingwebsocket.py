from tornado.websocket import WebSocketHandler
from tornado.escape import json_encode
from core.db import IntervalDocument, RunDocument, ModelDocument
from mongoengine import connect



class TrainingWebSocket(WebSocketHandler):
    connections = set()

    def get_compression_options(self):
        return {}

    def open(self, mgr_id):
        self.mgr_id = mgr_id
        self.connections.add(self)



        connect('neural_network')
        models = ModelDocument.objects(MGR_ID=mgr_id)

        if models.count() == 0:
            return

        model = models[0]

        model.status = 'finished' if model.run else 'running'

        res = {
            'MGR_ID': model.MGR_ID,
            'status': model.status
        }

        if model.status == 'finished':
            res['loss'] = model.run.loss
            res['accuracy'] = model.run.accuracy
            res['metadata'] = {
                'learningRate': float(model.run.learning_rate_exp),
                'dropoutRate': model.run.dropout_rate,
                'numSteps': model.run.num_steps,
                'trainingEpochs': model.run.training_epochs
            }

        self.write_message(res)

    def on_close(self):
        self.connections.remove(self)

    def check_origin(self, origin):
        return True

    @classmethod
    def send_model(cls, mgr_id, message):
        print('insidews')
        removable = set()

        for ws in cls.connections:
            if not ws.ws_connection or not ws.ws_connection.stream.socket:
                removable.add(ws)
            elif ws.mgr_id == mgr_id:
                ws.write_message(message)

        for ws in removable:
            TrainingWebSocket.connections.remove(ws)
