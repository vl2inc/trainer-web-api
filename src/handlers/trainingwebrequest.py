from concurrent.futures import ProcessPoolExecutor
from tornado.escape import json_encode
from tornado.web import RequestHandler
from core.process import start_training
from core.db import IntervalDocument, RunDocument, ModelDocument
from mongoengine import connect


executor = ProcessPoolExecutor(max_workers=1)


class TrainingWebRequest(RequestHandler):
    def get(self, mgr_id):
        connect('neural_network')
        models = ModelDocument.objects(MGR_ID=mgr_id)

        if models.count() == 0:
            self.set_status(404)
            self.finish()
            return

        model = models[0]

        model.status = 'finished' if model.run else 'running'

        #json =  json_encode(model)

        res = {
            'MGR_ID': model.MGR_ID,
            'status': model.status
        }

        if model.status == 'finished':
            res['loss'] = model.run.loss
            res['accuracy'] = model.run.accuracy
            res['metadata'] = {
                'learningRate': float(model.run.learning_rate_exp),
                'dropoutRate': model.run.dropout_rate,
                'numSteps': model.run.num_steps,
                'trainingEpochs': model.run.training_epochs
            }

        self.write(res)

    def post(self, mgr_id):
        print('Received POST training for:', mgr_id)
        executor.submit(start_training, mgr_id)

        self.finish()
