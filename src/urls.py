from handlers.trainingwebrequest import TrainingWebRequest
from handlers.trainingwebsocket import TrainingWebSocket
from handlers.socketwebrequest import SocketWebRequest


URLS = [
    (r'/api/models/(?P<mgr_id>[a-zA-Z0-9_.-]+)/training$', TrainingWebRequest),
    (r'/api/models/ws/(?P<mgr_id>[a-zA-Z0-9_.-]+)$', SocketWebRequest),
    (r'/ws/models/(?P<mgr_id>[a-zA-Z0-9_.-]+)$', TrainingWebSocket)
]
