from setuptools import setup, find_packages
from codecs import open
from os import path

HERE = path.abspath(path.dirname(__file__))

with open(path.join(HERE, 'README.md'), encoding='utf-8') as f:
    LONG_DESCRIPTION = f.read()

setup(
    name='trainerwebapi',
    version='0.1.0',
    description='Web API for the trainer module that triggers a model''s execution',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    packages=find_packages(),
    install_requires=[
        'django~=2.0.5',
        'djangorestframework~=3.8.2'
    ],
    python_requires='~=3.3'
)
