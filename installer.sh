#!/bin/bash
sudo yum install -y wget
sudo yum install -y unzip
sudo yum install -y gcc

# TODO: modify /etc/yum.repos.d/mongodb-org-3.6.repo
# TODO: sudo chkconfig mongod on

sudo yum install -y mongodb-org
sudo service mongod start

wget https://repo.anaconda.com/archive/Anaconda3-5.1.0-Linux-x86_64.sh -P ~/
sudo yum install -y bzip2
bash ~/Anaconda3-5.1.0-Linux-x86_64.sh -b -p $HOME/anaconda3
export PATH="$HOME/anaconda3/bin:$PATH"
echo 'export PATH="$HOME/anaconda3/bin:$PATH"' >> ~/.bashrc
sudo yum install -y git

#install kernels
conda install nb_conda_kernels -y

#create env
#conda create -y -n tensorflow pip python=3.6
#source activate tensorflow
#pip install --ignore-installed --upgrade https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-1.8.0-cp36-cp36m-linux_x86_64.whl
#conda install -y numpy pandas scikit-learn boto3 ipykernel

# Django REST framework
#pip install cython
#pip install django
#pip install djangorestframework
#pip install django-cors-headers
#pip install mongoengine
#pip install channel/etc/yum.repos.d/mongodb-org-3.6.repos
#pip install keras

