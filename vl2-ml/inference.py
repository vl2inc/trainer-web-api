
# coding: utf-8

# In[1]:


import tensorflow as tf
import numpy as np
from keras.models import load_model


# In[21]:


def build_dataset(test_num,test_cat) :
    x_test=[]
    x_test.append(np.array(test_num.as_matrix()))

    col_vals_dict = {c: list(test_cat[c].unique()) for c in test_cat.columns}

    embed_cols = []
    for c in col_vals_dict:
        embed_cols.append(c)
       
    #print(embed_cols)

    for c in embed_cols:
        ###############################################        
        
        x_test.append(np.asarray(test_cat[c]))
    #print (x_test)
    return x_test


# In[22]:


def predict(test_num,test_cat,model_path,tipo_modelo):
    model = load_model(model_path)
        
    x_test = build_dataset(test_num,test_cat)
    
    pred = np.round(model.predict(x_test))
    
    return pred


# In[23]:


#import pandas as pd

#test_cat = pd.read_csv('/home/ec2-user/trainer-web-api/vl2-ml/data/taxi/cat_test_data.csv',header=None)
#test_num = pd.read_csv('/home/ec2-user/trainer-web-api/vl2-ml/data/taxi/num_test_data.csv',header=None)    
#test_cat.iloc[2:3,:]

#test_cat=test_cat.iloc[2:3,:]
#test_num=test_num.iloc[2:3,:]

#test_cat.reset_index(drop=True,inplace=True)
#test_num.reset_index(drop=True,inplace=True)

#test_cat

#model_path= 'models/211a8588x5x22/211a8x1x1x1/model.h5'
#tipo_modelo='calsificator'

#predict(test_num,test_cat+1,model_path,tipo_modelo)

