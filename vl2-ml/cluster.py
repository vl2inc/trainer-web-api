
# coding: utf-8
#
# In[5]:


import numpy as np

def cluster (train_df, n_clusters = 5):

    from sklearn.cluster import KMeans
    x = train_df.iloc[:,1:].as_matrix()

    # Runs in parallel 4 CPUs
    kmeans = KMeans(n_clusters=n_clusters, n_init=20, n_jobs=4)
    # Train K-Means.
    y_pred_kmeans = kmeans.fit_predict(x)

    train_df['cluster'] = y_pred_kmeans
    
    
    return train_df



# In[6]:



